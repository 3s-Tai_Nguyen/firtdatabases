﻿using System;
using System.Collections.Generic;

namespace dbtai1.Models
{
    public partial class Student
    {
        public Student()
        {
            Enrollment = new HashSet<Enrollment>();
        }

        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstMidName { get; set; }
        public DateTime EnrollementDate { get; set; }

        public virtual ICollection<Enrollment> Enrollment { get; set; }
    }
}
